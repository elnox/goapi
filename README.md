# Goapi

A go api example with swagger ui

## Requirements

* go 1.17.x
* go get github.com/rakyll/statik

## How-to

### Build it

```bash
git clone https://gitlab.com/elnox/goapi.git
cd ./goapi && make
```

### Run it

```bash
./build/goapi help

Simple api example

Usage:
  api [flags]
  api [command]

Available Commands:
  help        Help about any command
  version     Show version

Flags:
  -c, --config string   config file to use (default "config.yaml")
  -h, --help            help for api

Use "api [command] --help" for more information about a command.
```

### Swagger

webui is by default available here

```
http://127.0.0.1:8888/swagger/
```

webui tweakings

```
# extract sources and set swagger to run live instead of statik
tar xpf www/swagger.tbz2 -C www/; sed -i -e 's/statik/live/' ./config.yaml

# make your changes, test, update www/v1/api.yaml openapi scheme if needed

# rebuild statik path and set swagger tu run statik
cd ./www/ && statik -f -src=$(pwd)/swagger; sed -i -e 's/live/statik/' ../config.yaml

# clean the repos
tar cjpf swagger.tbz2 ./swagger && rm -rf ./swagger
```

## Available endpoints

### Home

```bash
curl 127.0.0.1:8888/
```

### Api

```bash
curl -H "Authorization: Bearer <apikey>" 127.0.0.1:8888/v1/monip
```

### Metrics

```bash
curl 127.0.0.1:8888/metrics
```
