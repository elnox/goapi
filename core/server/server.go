package server

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/spf13/viper"

	log "github.com/sirupsen/logrus"
	_ "gitlab.com/elnox/goapi/www/statik"
)

// Define Server interface
type Server interface {
	StartRouter() error
	StopRouter(context.Context) error
}

// Define httpServer structure
type httpServer struct {
	*http.Server
}

// Http server constructor
func NewServer(host string, port int) Server {

	// Get timeouts from viper
	write := viper.GetInt("timeout.write")
	read := viper.GetInt("timeout.read")
	idle := viper.GetInt("timeout.idle")

	// Http server settings
	l := fmt.Sprintf("%s:%d", host, port)
	w := time.Second * time.Duration(write)
	r := time.Second * time.Duration(read)
	i := time.Second * time.Duration(idle)

	// Setup http server
	s := &http.Server{
		Addr:         l,
		WriteTimeout: w,
		ReadTimeout:  r,
		IdleTimeout:  i,
	}

	return &httpServer{s}
}

// Start http server and serve mux router
func (s *httpServer) StartRouter() error {

	// Mux router
	router := mux.NewRouter().StrictSlash(true)

	// Middlewares
	router.Use(s.metricMiddleware)
	router.Use(s.loggerMiddlware)

	// Http handler
	s.Handler = router

	// Setup basic routes
	router.HandleFunc("/", s.home)
	router.Handle("/metrics", promhttp.Handler())

	// Serve swagger ui
	s.swagger(router)

	// Populate routes
	s.populate(router)

	// Run http server in a goroutine to prevent block.
	go func() {
		if err := s.ListenAndServe(); err != nil {
			log.WithError(err).Warn("closing http server")
		}
	}()

	apikey := viper.GetString("global.apikey")
	log.Infof("⇨ http server started on %s", s.Addr)
	log.Infof("⇨ api key is %s", apikey)

	return nil
}

// Stop http server
func (s *httpServer) StopRouter(ctx context.Context) error {

	if err := s.Shutdown(ctx); err != nil {
		log.WithError(err).Warn("error while closing http server")
	}

	log.Infof("⇨ shutdown http server on %s", s.Addr)

	return nil
}
