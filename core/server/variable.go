package server

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

var (
	httpDuration = promauto.NewHistogramVec(prometheus.HistogramOpts{
		Name: "api_http_duration_seconds",
		Help: "Duration of HTTP requests.",
	}, []string{"path"})
)
