package server

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"strings"

	"github.com/gorilla/mux"
)

// Serve the root path of our http server
func (s *httpServer) home(w http.ResponseWriter, r *http.Request) {
	_, _ = fmt.Fprintf(w, "Welcome home!\n")
}

// Populate served routes
func (s *httpServer) populate(router *mux.Router) {
	// define subrouter path
	api := router.PathPrefix("/v1").Subrouter()

	// authent
	api.Use(s.authentMiddlware)

	// define api routes
	api.HandleFunc("", s.describe)
	api.HandleFunc("/monip", s.monip).Methods("GET")
}

// Serve /v1
func (s *httpServer) describe(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(
		w,
		fmt.Sprintf(
			"%s v%s @ %s\n",
			strings.Title("api"),
			"0.1",
			"localhost",
		),
	)
}

// Serve /v1/monip
func (s *httpServer) monip(w http.ResponseWriter, r *http.Request) {

	split := strings.Split(r.RemoteAddr, ":")

	ip := split[0]
	port, _ := strconv.Atoi(split[1])

	// new object
	monip := &Monip{
		Ip:     ip,
		Port:   port,
		Uri:    r.RequestURI,
		Method: r.Method,
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(monip)
}
