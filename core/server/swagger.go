package server

import (
	"net/http"

	"github.com/gorilla/mux"
	"github.com/rakyll/statik/fs"
	"github.com/spf13/viper"
)

// Serve swagger static files
func (s *httpServer) swagger(router *mux.Router) {
	var sh http.Handler
	switch viper.GetString("global.swagger") {
	case "statik":
		statikFS, err := fs.New()
		if err != nil {
			panic(err)
		}

		staticServer := http.FileServer(statikFS)
		sh = http.StripPrefix("/swagger/", staticServer)

	case "live":
		liveServer := http.FileServer(http.Dir("./www/swagger/"))
		sh = http.StripPrefix("/swagger/", liveServer)
	}

	router.PathPrefix("/swagger/").Handler(sh)
}
