package server

import (
	"context"
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/spf13/viper"

	log "github.com/sirupsen/logrus"
)

// Apikey token validator
func (s *httpServer) authentMiddlware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		apikey := viper.GetString("global.apikey")

		var header = r.Header.Get("Authorization")
		header = strings.TrimSpace(header)
		if header == "" {
			http.Error(w, "Forbidden", 403)
			return
		}

		if header != fmt.Sprintf("Bearer %s", apikey) {
			http.Error(w, "Access unauthorized", 401)
			return
		}

		ctx := context.WithValue(r.Context(), "token", apikey)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

// Metric middleware
func (s *httpServer) metricMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		route := mux.CurrentRoute(r)
		path, _ := route.GetPathTemplate()
		timer := prometheus.NewTimer(httpDuration.WithLabelValues(path))

		next.ServeHTTP(w, r)
		timer.ObserveDuration()
	})
}

// Logger middleware
func (s *httpServer) loggerMiddlware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		start := time.Now()
		next.ServeHTTP(w, r)
		latency := time.Since(start)

		log.Printf(
			"⇨ %s %s %s \"%s\" - %s",
			r.RemoteAddr,
			r.Method,
			r.RequestURI,
			r.Header.Get("User-Agent"),
			latency,
		)
	})
}
