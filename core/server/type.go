package server

type Monip struct {
	Ip     string `json:"ip,omitempty"`
	Port   int    `json:"port,omitempty"`
	Uri    string `json:"uri,omitempty"`
	Method string `json:"method,omitempty"`
}
