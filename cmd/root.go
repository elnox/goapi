package cmd

import (
	"context"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"

	log "github.com/sirupsen/logrus"

	"gitlab.com/elnox/goapi/core/server"
)

func Run() error {
	return rootCmd.Execute()
}

var rootCmd = &cobra.Command{
	Use:   App,
	Short: "Simple api example",
	Run:   rootFn,
}

func rootFn(cmd *cobra.Command, args []string) {

	// Get server config
	host := viper.GetString("server.host")
	port := viper.GetInt("server.port")

	// New server object
	srv := server.NewServer(host, port)

	// Start router
	if err := srv.StartRouter(); err != nil {
		log.WithError(err).Fatal("cannot start http server")
	}

	// Setting up signal capturing
	quit := make(chan os.Signal, 3)

	signal.Notify(quit, os.Interrupt)
	signal.Notify(quit, syscall.SIGTERM)
	signal.Notify(quit, syscall.SIGINT)

	// Waiting for signal
	<-quit

	// Stop router with a max delay of 5 sec
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	if err := srv.StopRouter(ctx); err != nil {
		log.WithError(err).Fatal("cannot stop http server")
	}
}
