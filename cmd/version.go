package cmd

import (
	"fmt"
	"runtime"

	"github.com/spf13/cobra"
)

func init() {
	rootCmd.AddCommand(versionCmd)
}

var versionCmd = &cobra.Command{
	Use:   "version",
	Short: "Show version",
	Run:   versionFn,
}

func versionFn(cmd *cobra.Command, args []string) {
	fmt.Printf("%s v%s - %s\n", App, Version, Githash)
	fmt.Printf("build date %s\n", BuildDate)
	fmt.Printf("runtime %s %s/%s\n", runtime.Version(), runtime.GOOS, runtime.GOARCH)
}
