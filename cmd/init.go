package cmd

import (
	"github.com/spf13/cobra"
	"github.com/spf13/viper"

	log "github.com/sirupsen/logrus"
)

func init() {

	cobra.OnInitialize(initConfig)

	pf := rootCmd.PersistentFlags()
	pf.StringP("config", "c", "config.yaml", "config file to use")

	err := viper.BindPFlags(pf)
	if err != nil {
		log.WithError(err).Fatal("fail to bind persistent flags")
	}
}

func initConfig() {

	viper.SetDefault("global.log-level", 4)
	viper.SetDefault("global.swagger", swagger)
	viper.SetDefault("global.apikey", apikey)
	viper.SetDefault("server.host", host)
	viper.SetDefault("server.port", port)
	viper.SetDefault("timeout.write", 10)
	viper.SetDefault("timeout.read", 10)
	viper.SetDefault("timeout.write", 60)

	viper.SetEnvPrefix(App)
	viper.AutomaticEnv()

	viper.AddConfigPath("/etc/" + App + "/")
	viper.AddConfigPath("$HOME/." + App)
	viper.AddConfigPath(".")

	log.SetFormatter(
		&log.TextFormatter{
			FullTimestamp:   true,
			TimestampFormat: "2006-01-02 15:04:05.000000",
		},
	)

	config := viper.GetString("config")
	if "" != config {
		viper.SetConfigFile(config)
	} else {
		viper.SetConfigName("config")
	}

	if err := viper.ReadInConfig(); err != nil {
		log.Fatalf("error config file: %v", err)
	}

	level := viper.GetInt("global.log-level")
	log.SetLevel(log.InfoLevel)
	if level >= 0 && level < len(log.AllLevels) {
		log.SetLevel(log.AllLevels[level])
	}
}
