package cmd

import "github.com/gofrs/uuid"

const (
	App     = "api"
	Version = "0.1"
)

var (
	Githash   = "HEAD"
	BuildDate = "1970-01-01T00:00:00Z UTC"
	apikey, _ = uuid.NewV4()
	swagger   = "statik"
	host      = "localhost"
	port      = 8888
)
